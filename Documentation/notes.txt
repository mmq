hacking notes
=============

audio output
------------

* usually stays open between songs if song format doesn't change
* automatically closed on error
* automatically opened/reopened when needed
* sets state to advance to next song on error

file input
----------

* open and closed explicitly with each song
* does not automatically close on error
* sets state to advance to next song on error
