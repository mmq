controlling mmq
===============

mmq can take exactly three commands:

1. enqueue a song
2. set seek point for the next song in the queue
3. skip the currently playing song

Enqueueing (1) and seek points (2) are accomplished synchronously via
POSIX message queues.  Skipping (3) is accomplished by sending a USR1
signal to the player.

POSIX message queue protocol
----------------------------

The primary mechanism for queueing songs and setting seek points
for the next song in the queue is done via POSIX message queues.

To queue a song, send a message into the message queue (default: "/mmq")
with the path name prefixed with a '=' (equal sign, byte 0x3D).

	=/path/to/file

Path names are always taken as-is.  No character conversion or expansion
on pathnames is ever performed.  If a relative pathname is sent, then
it is opened relative to where mmq is running.  mmq will never change
its working directory from where it started.


Setting a seek point for the next song is accomplished by setting
an offset (in milliseconds) to start the next song at.  To set a
seek point, just prefix the offset with ':' (colon, byte 0x3A).

	:MILLISECONDS

Thats it!


Building blocks
---------------

Common playback tasks found in most music players can be accomplished
through other means.

* pause
  1. open the POSIX message queue in a separate application and clear it
  2. send USR1 to the player to skip to the next song
