Anti-Features: things mmq will never support (and why)
======================================================

Unlike most music players, mmq is defined by the *lack* of features.
mmq only tries to support the bare essentials for playing audio, giving
users the choice to integrate it with their own helper scripts or
applications.

The following things will never be supported in mainline mmq:

* filesystem writes - mmq will never write to regular files on your
  filesystem.  If you need things like logging, playlists, or databases,
  they are better handled by external applications that specialize in
  those things.  The only exception is stdout or stderr being redirected
  by the calling process (shell).

* playlists - mmq is strictly a queue, once a file is played, it must be
  requeued to be played again, there is no way to play arbitrary
  songs within the queue, only to advance to the next song.  It is up
  to the controlling application to manage the queue.

* metadata editing - There are plenty of good metadata viewers and
  editors out there, and some folks just use descriptive path names.
  Again, explicit filesystem writes will never be supported.

* non-regular-filesystem reads - mmq will only ever contain code to read
  regular files from POSIX-like filesystems.

* volume/mixer control - There are too many different hardware setups
  out there.  It's too much work to support all of them well, so we'll
  support none of them instead.  Most everybody already has a separate
  volume control application anyways.

* software volume - most people have hardware mixers or can
  emulate that mixing within their sound daemons or libraries.

* ReplayGain - Better to fix audio in the first place or not
  support music that participates in the loudness war.  Real-time audio
  processing during playback is expensive and we lack the patience to
  deal with software volume mixers.

* equalizer - see ReplayGain

* parsing configuration files - all configuration is done at build time,
  the GNU Make-based build system should be easy to customize to your
  needs.

* internationalization/encoding support - POSIX filesystems are just
  a bag of bytes and we won't care about normalization or character
  set conversions.

* embedded CUE sheets, archive files - use FUSE or something similar
  to mount them, or split the files and use filesystems like they
  were intended.

* metadata management/searching - this is not a media library, we
  will not tell you how to organize your music.

Of course mmq is Free Software licensed under the GPLv3, so feel free to
fork it and hack it to your desires, just make it clear that it's a fork
or rename it.
