% MMQ-PLAYER(1) Minimalist Music Queue Manual
% Eric Wong <normalperson@yhbt.net>
% January 23, 2010

# NAME

mmq-player - audio player for the minimalist music queue

# SYNOPSIS

mmq-player

# DESCRIPTION

The core of mmq, this plays music in the message queue.  It is very
efficient (using mmap(2) with ALSA) and capable of gap-free playback of
FLAC and Ogg-Vorbis formats.

# OPTIONS

mmq-player is completely configured at build-time and accepts no runtime
configuration options.

# SIGNALS

SIGUSR1 to skip the currently playing track and advance to the next
track in the queue.  SIGTERM or SIGINT should be used to terminate
and stop playback.

# INPUT

mmq-player reads from POSIX message queues, see [`mmq_protocol(7)`]
The default POSIX message queue used is "/mmq".

# OUTPUT

Human-readable warnings and errors are emitted to stderr.
Machine-parseable protocol to stdout described in [`mmq_protocol(7)`]

# ENVIRONMENT

Environment variables are not parsed.

# SEE ALSO

[`mmq-enq`(1)]
[`mmq`(7)]
[`mmq_protocol`(7)]

# CAVEATS

Only plays 2 channel, 16 bit audio.

Extremely hardware and system-dependent for gap-free playback, there is
no userspace buffering used at all to minimize data copies.  This also
makes it susceptible to skips with insufficient buffering in hardware
and kernel.

# CONTACT

Mailing list: mmq@librelist.com
Eric Wong: normalperson@yhbt.net
