/*
 * PulseAudio support
 *
 * This file is meant to be included in ao.c
 */
#include <pulse/simple.h>
#include <pulse/error.h>
#include <stdlib.h>

static pa_simple *s;
static pa_sample_spec ss;

static pa_sample_format_t bitformat(unsigned bits)
{
	switch (bits) {
	case 8: return PA_SAMPLE_U8;
	case 16: return PA_SAMPLE_S16NE;
#ifdef PA_SAMPLE_S24NE
	case 24: return PA_SAMPLE_S24NE;
#endif
	case 32: return PA_SAMPLE_S32NE;
	}
	return PA_SAMPLE_INVALID;
}


static int _my_open(struct afmt *fmt)
{
	pa_sample_format_t bitfmt = bitformat(fmt->bits);

	if (bitfmt == PA_SAMPLE_INVALID) {
		warn("unknown bitrate: %u\n", fmt->bits);
		return -1;
	}

	ss.format = bitfmt;
	ss.rate = fmt->rate;
	ss.channels = fmt->channels;
	g_sample_size = (fmt->bits / CHAR_BIT) * fmt->channels;
	s = pa_simple_new(NULL, "mmq", PA_STREAM_PLAYBACK, NULL,
			  "minimalist music queue", &ss, NULL, NULL, NULL);

	return s ? 0 : -1;
}

static void _my_close(int graceful)
{
	int error, rc;

	if (!s)
		return;
	if (graceful)
		rc = pa_simple_drain(s, &error);
	else
		rc = pa_simple_flush(s, &error);
	if (rc < 0)
		warn("drain or flush failed: %s\n", pa_strerror(error));

	pa_simple_free(s);
	s = NULL;
}

static int _my_inject(void *buf, long samples, conv_fn conv_i)
{
	size_t bytes = samples * g_sample_size;
	void *data;
	int error;

	if (bytes == 0)
		return 0;

	if (conv_i == audio_copy) {
		data = buf;
	} else {
		static size_t capa;
		static void *tmp;
		long nr;

		if (bytes > capa) {
			capa = bytes;
			tmp = realloc(tmp, bytes);
			assert(tmp && "realloc failed, wtf");
		}

		nr = conv_i(tmp, buf, samples, 0);
		if (nr == 0) /* OV_HOLE */
			return 0;
		assert(nr > 0 && "nr != 0");
		bytes = nr * g_sample_size;
		data = tmp;
	}
	if (pa_simple_write(s, data, bytes, &error) < 0) {
		warn("pa_simple_write failed: %s\n", pa_strerror(error));
		return -1;
	}
	return samples;
}
