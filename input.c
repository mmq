#include "mmq.h"
#include <sys/mman.h>

/* all mmq code to read audio files is here */

/* we only open one file at a time */
static int fd = -1;
static struct stat sb;
static void *mm;

/* current (early 2010) Linux doesn't handle POSIX_FADV_NOREUSE */
static int force_noreuse
#if defined(__linux__)
= 1
#endif
;

#ifndef SIZE_T_MAX
#  define SIZE_T_MAX ((size_t)-1)
#endif

#ifndef PAGE_SIZE
#  if defined(__linux__) && (defined(__i386__) || defined(__x86_64__))
#    define PAGE_SIZE (4096)
#  else
#    define PAGE_SIZE (sysconf(_SC_PAGESIZE))
#  endif
#endif
#define PAGE_MASK (~(PAGE_SIZE - 1))
#define PAGE_ALIGN(addr) (((addr) + PAGE_SIZE - 1) & PAGE_MASK)

/*
 * Linux madvise(2) doesn't map exactly to posix_madvise for DONTNEED,
 * but the difference does not matter for read-only mappings.
 */
#ifdef __linux__
#  define madv_dontneed(addr,len) madvise(addr,len,MADV_DONTNEED)
#else
#  define madv_dontneed(addr,len) posix_madvise(addr,len,POSIX_MADV_DONTNEED)
#endif

static void init_readahead(off_t offset)
{
	/* no error checking here, these are only optimizations */
	(void)posix_fadvise(fd, offset, sb.st_size, POSIX_FADV_NOREUSE);
	(void)posix_fadvise(fd, offset, sb.st_size, POSIX_FADV_SEQUENTIAL);
}

int input_open(const char *path)
{
	const char *err;
	int flags = O_RDONLY | O_NOATIME;

	sb.st_size = -1;

retry:
	fd = open(path, flags);
	if (fd < 0) {
		if (flags & O_NOATIME) {
			flags = O_RDONLY;
			goto retry;
		}
		err = "open";
	} else if (fstat(fd, &sb) < 0) {
		err = "fstat";
	} else {
		if (g_seek_msec == 0)
			init_readahead(0);
		return 0;
	}

	warn("%s: %s\n", err, strerror(errno));
	fd = -1;
	return -1;
}

/*
 * this may only be called once per song
 * mmq will never be able to seek once playback is started
 * (frontends that want to can implement this feature trivially)
 */
off_t input_seek(off_t offset, int whence)
{
	off_t off = lseek(fd, offset, whence);

	if (off < 0) {
		if (errno != ESPIPE) {
			warn("lseek: %s\n", strerror(errno));
			g_state = STATE_NEXT;
		}
	} else {
		init_readahead(off);
	}

	return off;
}

off_t input_size(void)
{
	if (sb.st_size < 0)
		g_state = STATE_NEXT;
	return sb.st_size;
}

/*
 * FLAC/Tremor currently require a read()-like interface, we use read()
 * instead of mmap()+memcpy() since setting up and maintaining memory
 * mappings are still expensive and generally outweigh the cost of the
 * copying and extra syscalls.
 */
ssize_t input_read(void *buf, size_t count)
{
	ssize_t r;
	if (likely(g_state == STATE_PLAY)) {
		r = read(fd, buf, count);
		if (likely(r >= 0))
			return r;

		warn("read: %s\n", strerror(errno));
		g_state = STATE_NEXT;
		return r;
	} else if (g_state == STATE_NEXT) {
		return -1;
	}
	assert(0 && "unknown g_state, should never get here");
}

/*
 * mmap won't work on MMU-less machines since they can't mmap, but since
 * only MAD uses this for MP3, and MP3s are reasonably small maybe we
 * could just slurp the whole thing into memory (and refuse to play MP3s
 * we can't slurp).
 */
/* TODO: seek awareness */
void *input_mmap(size_t *size)
{
	off_t length;

	/* already mmap-ed, consider this an EOF */
	if (mm)
		return NULL;

	length = PAGE_ALIGN(sb.st_size);
	if (length > SIZE_T_MAX) {
		warn("file too large for mmap\n");
		goto err;
	}

	mm = mmap(NULL, length, PROT_READ, MAP_PRIVATE, fd, 0);
	if (mm == MAP_FAILED) {
		warn("mmap: %s\n", strerror(errno));
		goto err;
	}

	*size = sb.st_size;
	(void)posix_madvise(mm, length, POSIX_MADV_SEQUENTIAL);

	return mm;
err:
	*size = -1;
	g_state = STATE_NEXT;
	return NULL;
}

void input_close(void)
{
	if (mm) {
		(void)madv_dontneed(mm, PAGE_ALIGN(sb.st_size));
		if (munmap(mm, sb.st_size) < 0)
			warn("munmap: %s\n", strerror(errno));
		mm = NULL;
	}
	if (force_noreuse)
		(void)posix_fadvise(fd, 0, sb.st_size, POSIX_FADV_DONTNEED);
	if (close(fd) < 0)
		warn("close: %s\n", strerror(errno));

	fd = -1;
}
