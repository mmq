#include "fixeddec.h"
#include <tremor/ivorbisfile.h>

static size_t read_cb(void *ptr, size_t size, size_t nmemb, UNUSED void *x)
{
	return (size_t)input_read(ptr, size * nmemb);
}

static int seek_cb(UNUSED void *x, ogg_int64_t offset, int whence)
{
	return (int)input_seek((off_t)offset, whence);
}

static long tell_cb(UNUSED void *x)
{
	return (long)input_seek(0, SEEK_CUR);
}

static int close_cb(UNUSED void *x)
{
	return 0;
}

static const ov_callbacks callbacks = {
	.read_func = read_cb,
	.seek_func = seek_cb,
	.close_func = close_cb,
	.tell_func = tell_cb,
};

static char buf[4096];
static OggVorbis_File vf;
static int current_bitstream, bitstream;

static long tremor_cp(void *dst, UNUSED void *s, long samples, UNUSED long o)
{
	long rv = ov_read(&vf, dst, samples * g_sample_size, &bitstream);

	if (rv >= 0)
		return rv / g_sample_size;
	return (rv == OV_HOLE) ? 0 : rv;
}

static int open_audio(void)
{
	vorbis_info *vi = ov_info(&vf, -1);

	g_next_afmt.rate = vi->rate;
	g_next_afmt.channels = vi->channels;
	g_next_afmt.bits = 16;

	return audio_open();
}

void fixeddec_play_tremor(void)
{
	long rv = ov_open_callbacks(&rv, &vf, NULL, 0, callbacks);
	long samples;

	if (rv < 0) {
		warn("ov_open_callbacks: %ld\n", rv);
		return;
	}

	if (g_seek_msec > 0) {
		rv = ov_time_seek_page(&vf, g_seek_msec);
		g_seek_msec = 0;
		if (rv != 0)
			goto out;
	}

	rv = ov_read(&vf, buf, sizeof(buf), &bitstream);
	if (rv < 0) {
		if (rv != OV_HOLE)
			goto out;
		rv = 0;
	}

	if (open_audio() < 0)
		goto out;
	if (rv > 0 && audio_inject(buf, rv / g_sample_size, audio_copy) < 0)
		goto out;

	samples = sizeof(buf) / g_sample_size;
	current_bitstream = bitstream;
	while (likely(g_state == STATE_PLAY)) {
		if (audio_inject(NULL, samples, tremor_cp) <= 0)
			break;
		if (unlikely(current_bitstream != bitstream)) {
			current_bitstream = bitstream;
			if (open_audio() < 0)
				break;
			samples = sizeof(buf) / g_sample_size;
		}
	}
out:
	ov_clear(&vf);
}
