#ifndef FIXEDDEC_H
#define FIXEDDEC_H
#include "../mmq.h"

extern void fixeddec_play_flac(void);
extern void fixeddec_play_tremor(void);
extern void fixeddec_play_mad(void);

#define SUFFIX_MATCH(buf,len,suf) \
  (len >= (sizeof(suf)) && \
   ! memcmp(buf + len - (sizeof(suf)), "."suf, sizeof(suf)))

static inline void fixeddec_play(const char *buf, size_t len)
{
	if (input_open(buf) == 0) {
		/* CASE-INSENSITIVITY IS A DISEASE */
		if (SUFFIX_MATCH(buf, len, "ogg"))
			fixeddec_play_tremor();
		else if (SUFFIX_MATCH(buf, len, "flac"))
			fixeddec_play_flac();
		else if (SUFFIX_MATCH(buf, len, "mp3"))
			fixeddec_play_mad();
		else
			warn("unknown suffix\n");

		input_close();
	}
}

#endif /* FIXEDDEC_H */
