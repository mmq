#include "fixeddec.h"
#include <FLAC/export.h>
#include <FLAC/stream_decoder.h>

static void error_cb(
	UNUSED const FLAC__StreamDecoder *dec,
	FLAC__StreamDecoderErrorStatus status,
	UNUSED void *data)
{
	g_state = STATE_NEXT;
	if (status < 4)
		warn("FLAC error: %s\n",
		     FLAC__StreamDecoderErrorStatusString[status]);
}

static FLAC__StreamDecoderTellStatus tell_cb(
	UNUSED const FLAC__StreamDecoder *dec,
	FLAC__uint64 *offset,
	UNUSED void *data)
{
	off_t off = input_seek(0, SEEK_CUR);

	if (off >= 0) {
		*offset = off;
		return FLAC__STREAM_DECODER_LENGTH_STATUS_OK;
	}
	if (errno == ESPIPE)
		return FLAC__STREAM_DECODER_LENGTH_STATUS_UNSUPPORTED;
	return FLAC__STREAM_DECODER_LENGTH_STATUS_ERROR;
}

static FLAC__StreamDecoderSeekStatus seek_cb(
	UNUSED const FLAC__StreamDecoder *dec,
	FLAC__uint64 offset,
	UNUSED void *data)
{
	off_t rv = input_seek(offset, SEEK_SET);

	if (rv >= 0)
		return FLAC__STREAM_DECODER_SEEK_STATUS_OK;
	if (errno == ESPIPE)
		return FLAC__STREAM_DECODER_SEEK_STATUS_UNSUPPORTED;
	return FLAC__STREAM_DECODER_SEEK_STATUS_ERROR;
}

static FLAC__StreamDecoderLengthStatus length_cb(
	UNUSED const FLAC__StreamDecoder *dec,
	FLAC__uint64 *length,
	UNUSED void *data)
{
	*length = (FLAC__uint64)input_size();

	if (*length > 0)
		return FLAC__STREAM_DECODER_LENGTH_STATUS_OK;
	if (*length == 0)
		return FLAC__STREAM_DECODER_LENGTH_STATUS_UNSUPPORTED;
	return FLAC__STREAM_DECODER_LENGTH_STATUS_ERROR;
}

static void metadata_cb(
	UNUSED const FLAC__StreamDecoder *dec,
	const FLAC__StreamMetadata *block,
	UNUSED void *data)
{
	const FLAC__StreamMetadata_StreamInfo *si;

	if (block->type != FLAC__METADATA_TYPE_STREAMINFO)
		return;
	si = &block->data.stream_info;
	g_next_afmt.rate = si->sample_rate;
	g_next_afmt.channels = si->channels;
	g_next_afmt.bits = si->bits_per_sample;

	(void)audio_open(); /* can't error check */
	return;
}

/*
 * converts 16-bit, 2-ch audio from 32-bit non-interleaved chunks
 * to 16-bit interleaved chunks.
 */
static long conv_16_2_i(
	void *restrict dst, void *restrict _src, long samples, long offset)
{
	int32_t **src = _src;
	int32_t *left = src[0] + offset;
	int32_t *right = src[1] + offset;
	uint16_t *tmp = dst;
	long rv = samples;

	for (; samples-- != 0; ) {
		*tmp++ = *left++;
		*tmp++ = *right++;
	}

	return rv;
}

/*
 * converts 24 or 32-bit, 2-ch audio from 32-bit non-interleaved chunks
 * to 32-bit interleaved chunks.
 */
static long conv_32_2_i(
	void *restrict dst, void *restrict _src, long samples, long offset)
{
	int32_t **src = _src;
	int32_t *left = src[0] + offset;
	int32_t *right = src[1] + offset;
	uint32_t *tmp = dst;
	long rv = samples;

	for (; samples-- != 0; ) {
		*tmp++ = *left++;
		*tmp++ = *right++;
	}

	return rv;
}

/*
 * 24/32-bit FLAC samples may be played non-interleaved for maximum
 * performance (at least on mmq-player, the underlying sound architecture
 * may still do this conversion...
 */
static FLAC__StreamDecoderWriteStatus write_cb(
	UNUSED const FLAC__StreamDecoder *dec,
	const FLAC__Frame *frame,
	const FLAC__int32 * const buf[],
	UNUSED void *data)
{
	unsigned samples = frame->header.blocksize;
	union {
		const FLAC__int32 * const * buf;
		void *blob;
	} deconst;
	int w;
	conv_fn conv_i;

	if (frame->header.channels != 2) {
		warn("Only 2 channel output is supported (channels=%u)\n",
		     frame->header.channels);
		return FLAC__STREAM_DECODER_WRITE_STATUS_ABORT;
	}
	switch (frame->header.bits_per_sample) {
	case 16: conv_i = conv_16_2_i; break;
	case 24:
	case 32: conv_i = conv_32_2_i; break;
	default:
		warn("Only 16, 24, or 32-bit output is supported (bits=%u)\n",
		     frame->header.bits_per_sample);
		return FLAC__STREAM_DECODER_WRITE_STATUS_ABORT;
	}

	deconst.buf = buf;
	w = audio_inject(deconst.blob, samples, conv_i);
	if (w >= 0)
		return FLAC__STREAM_DECODER_WRITE_STATUS_CONTINUE;

	return FLAC__STREAM_DECODER_WRITE_STATUS_ABORT;
}

static FLAC__StreamDecoderReadStatus read_cb(
	UNUSED const FLAC__StreamDecoder * dec,
	FLAC__byte buf[],
	size_t *bytes,
	UNUSED void *data)
{
	ssize_t r = input_read(buf, *bytes);

	if (r > 0)
		return FLAC__STREAM_DECODER_READ_STATUS_CONTINUE;
	else if (r == 0)
		return FLAC__STREAM_DECODER_READ_STATUS_END_OF_STREAM;
	else
		return FLAC__STREAM_DECODER_READ_STATUS_ABORT;
}

static FLAC__bool flac_seek(FLAC__StreamDecoder *sd)
{
	FLAC__uint64 dest;

	if (!g_seek_msec)
		return 1;

	dest = g_seek_msec * (g_next_afmt.rate / 1000);
	g_seek_msec = 0;

	return FLAC__stream_decoder_seek_absolute(sd, dest);
}

static FLAC__bool eof_cb(
	UNUSED const FLAC__StreamDecoder * dec,
	UNUSED void *data)
{
	return false;
}

void fixeddec_play_flac(void)
{
	FLAC__StreamDecoder *sd = FLAC__stream_decoder_new();
	FLAC__bool rv;

	rv  = FLAC__stream_decoder_init_stream(
			sd,
			read_cb, seek_cb, tell_cb,
			length_cb, eof_cb, write_cb,
			metadata_cb, error_cb, NULL);

	if (rv != FLAC__STREAM_DECODER_SEARCH_FOR_METADATA)
		goto ex;
	if (!FLAC__stream_decoder_process_until_end_of_metadata(sd))
		goto ex;
	if (!flac_seek(sd))
		goto ex;
	FLAC__stream_decoder_process_until_end_of_stream(sd);
ex:
	FLAC__stream_decoder_delete(sd);
}
