/*
 * this file contains dithering code from madplay
 *
 * Copyright (C) 2000-2004 Robert Leslie
 * Copyright (C) 2010 Eric Wong
 *
 * madplay is GPLv2+, and most of mmq is GPLv3 (only) for now.
 * If newer versions of the GPL come out, Eric Wong reserves the right
 * to upgrade to a newer version of the GPL.
 */

/*
 * This is the least of feature complete of the libfixeddec decoders.
 * Then again, corner cases are not so important to us, so things will
 * likely stay this way unless somebody provides patches and most
 * importantly, promises to maintain it.
 *
 * - Gapless playback is not handled at all.  This is a flaw in the MP3
 *   format itself.  There are hacks implemented by various encoders
 *   (LAME and Xing at least) which allow the decoder to skip over these
 *   gaps.  Since gapless is mostly needed for live audio or
 *   album-oriented music where FLAC is king anyways, it's no big deal
 *
 * - Seeking does not work.  We're using the stupid simple libMAD API,
 *   not the more powerful one, so there doesn't appear to be a way to
 *   support it without drastically increasing our own code complexity.
 *   Both Tremor and libFLAC manage to have easy-to-use APIs that
 *   support seeking.
 */

#include "fixeddec.h"
#include <mad.h>
static int audio_opened;

struct dither {
	mad_fixed_t error[3];
	mad_fixed_t random;
};

/* pseudo-random number generator */
static unsigned long prng(unsigned long state)
{
	return (state * 0x0019660dL + 0x3c6ef35fL) & 0xffffffffL;
}

/* generic linear sample quantize and dither routine */
static long audio_linear_dither(
	unsigned bits,
	mad_fixed_t sample,
	struct dither *dither)
{
	unsigned scalebits;
	mad_fixed_t output, mask, rnd;
	enum {
		MIN = -MAD_F_ONE,
		MAX = MAD_F_ONE - 1
	};

	/* noise shape */
	sample += dither->error[0] - dither->error[1] + dither->error[2];

	dither->error[2] = dither->error[1];
	dither->error[1] = dither->error[0] / 2;

	/* bias */
	output = sample + (1L << (MAD_F_FRACBITS + 1 - bits - 1));

	scalebits = MAD_F_FRACBITS + 1 - bits;
	mask = (1L << scalebits) - 1;

	/* dither */
	rnd = prng(dither->random);
	output += (rnd & mask) - (dither->random & mask);

	dither->random = rnd;

	/* clip */
	if (output > MAX) {
		output = MAX;
		if (sample > MAX)
			sample = MAX;
	} else if (output < MIN) {
		output = MIN;
		if (sample < MIN)
			sample = MIN;
	}

	/* quantize */
	output &= ~mask;

	/* error feedback */
	dither->error[0] = sample - output;

	/* scale */
	return output >> scalebits;
}

/*
 * dithers 16-bit, 2-ch audio from 32-bit non-interleaved
 * chunks to 16-bit interleaved chunks.
 */
static long dither_16_2_i(
	void *restrict dst, void *restrict src, long samples, long offset)
{
	struct mad_pcm *pcm = src;
	mad_fixed_t const *left = pcm->samples[0] + offset;
	mad_fixed_t const *right = pcm->samples[1] + offset;
	int16_t *tmp = dst;
	static struct dither d[2];
	long rv = samples;

	for (; samples-- != 0; ) {
		*tmp++ = audio_linear_dither(16, *left++, d);
		*tmp++ = audio_linear_dither(16, *right++, d + 1);
	}

	return rv;
}

static enum mad_flow output(
	UNUSED void *data,
	UNUSED struct mad_header const *header,
	struct mad_pcm *pcm)
{
	if (unlikely(! audio_opened)) {
		g_next_afmt.rate = pcm->samplerate;
		g_next_afmt.channels = pcm->channels;
		g_next_afmt.bits = 16;
		if (audio_open() < 0)
			return MAD_FLOW_BREAK;
		audio_opened = 1;
	}

	if (audio_inject(pcm, pcm->length, dither_16_2_i) > 0)
		return MAD_FLOW_CONTINUE;
	return MAD_FLOW_BREAK;
}

static enum mad_flow error(
	UNUSED void *data,
	struct mad_stream *stream,
	UNUSED struct mad_frame *frame)
{
	if (MAD_RECOVERABLE(stream->error))
		return MAD_FLOW_CONTINUE;

	warn("MAD error: %s\n", mad_stream_errorstr(stream));

	return MAD_FLOW_BREAK;
}

/*
 * We don't do any intelligent buffer management here, just mmap the
 * entire file and let the kernel deal with it.  We're less likely
 * to have bugs due to bad math in buffer management this way.
 */
static enum mad_flow input(UNUSED void *data, struct mad_stream *stream)
{
	size_t nr = 0;
	void *mm = input_mmap(&nr);

	if (!mm)
		return (nr == 0) ? MAD_FLOW_STOP : MAD_FLOW_BREAK;

	mad_stream_buffer(stream, mm, nr);

	return MAD_FLOW_CONTINUE;
}

void fixeddec_play_mad(void)
{
	static void *data, *header, *filter, *message;
	struct mad_decoder decoder;

	if (g_seek_msec) {
		warn("seeking is not supported for MAD\n");
		g_seek_msec = 0;
	}

	mad_decoder_init(&decoder, data,
	                 input, header, filter, output, error, message);

	(void)mad_decoder_run(&decoder, MAD_DECODER_MODE_SYNC);
	(void)mad_decoder_finish(&decoder);

	audio_opened = 0;
}
