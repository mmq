/* MMQ_AO_IMPL should be defined by the Makefile */
#ifndef MMQ_AO_IMPL
#  error MMQ_AO_IMPL not defined
#endif
#include "mmq.h"

struct afmt g_next_afmt;
static struct afmt current;
unsigned long g_sample_size;

static int matches_current(void)
{
	/* no using memcmp because of potential padding */
	return ((g_next_afmt.rate == current.rate) &&
	        (g_next_afmt.bits == current.bits) &&
		(g_next_afmt.channels == current.channels));
}

/* this should statically define _my_open, _my_close */
#include MMQ_AO_IMPL

int audio_open(void)
{
	int rv;

	if (matches_current())
		return 0;
	if (g_next_afmt.channels != 2) {
		warn("only 2 channel output is supported for now\n");
		g_state = STATE_NEXT;
		return -1;
	}
	audio_close(1);

	rv = _my_open(&g_next_afmt);
	if (rv < 0) {
		warn("ao_open failed\n");
		g_state = STATE_NEXT;
	} else {
		memcpy(&current, &g_next_afmt, sizeof(struct afmt));
		emit("a%u:%u:%u\n",
		     current.rate, current.bits, current.channels);
	}

	return rv;
}

void audio_close(int graceful)
{
	_my_close(graceful);
	memset(&current, 0, sizeof(struct afmt));
}

int audio_inject(void *buf, long samples, conv_fn conv_i)
{
	if (likely(g_state == STATE_PLAY))
		return _my_inject(buf, samples, conv_i);

	return -1;
}

long audio_copy(void *dst, void *src, long samples, long offset)
{
	src = (char *)src + (offset * g_sample_size);
	memcpy(dst, src, samples * g_sample_size);

	return samples;
}
