# defaults for SALSA and ALSA, meant to be included in Makefile

MMQ_AO_PREFIX ?= $(prefix)
MMQ_AO_LIBS ?= -lsalsa
MMQ_AO_LDFLAGS ?= -L$(MMQ_AO_PREFIX)/lib
MMQ_AO_CFLAGS ?= -I$(MMQ_AO_PREFIX)/include

# ALSA device, raw ALSA/SALSA support only supports writing to one device
# MMQ_ALSA_DEVICE = default
ifdef MMQ_ALSA_DEVICE
  # default is "default", values like "hw:1,0" (unquoted) are accepted here
  MMQ_AO_CFLAGS += -DMMQ_ALSA_DEVICE='"$(MMQ_ALSA_DEVICE)"'
endif

# we disable auto-{resampling/channels/format} by default,
# MMQ_ALSA_MODE = NO_AUTO_RESAMPLE NO_AUTO_CHANNELS NO_AUTO_FORMAT
ifdef MMQ_ALSA_MODE
  space =
  space +=
  alsa_mode := $(subst $(space),|,$(addprefix SND_PCM_,$(MMQ_ALSA_MODE)))
  MMQ_AO_CFLAGS += -DMMQ_ALSA_MODE='$(alsa_mode)'
endif

# integer time in microseconds. increasing this can reduce skipping.
# Setting it to zero will use the device default (which may not work with mmap)
# The mmq default is 8 seconds
#   MMQ_ALSA_BUFFER_TIME = 8000000
ifdef MMQ_ALSA_BUFFER_TIME
  MMQ_AO_CFLAGS += -DMMQ_ALSA_BUFFER_TIME=$(MMQ_ALSA_BUFFER_TIME)
endif

# integer time in microseconds. increasing this will lower CPU usage
# Setting this to zero will use the default (which may not work with mmap)
# The mmq default is 10 milliseconds
#   MMQ_ALSA_BUFFER_TIME = 10000
ifdef MMQ_ALSA_PERIOD_TIME
  MMQ_AO_CFLAGS += -DMMQ_ALSA_PERIOD_TIME=$(MMQ_ALSA_PERIOD_TIME)
endif

# some helper targets to make custom/static distributions easier
salsa_dir = salsa-lib-0.0.26
salsa_pkg = $(salsa_dir).tar.bz2
salsa_url = ftp://ftp.suse.com/pub/people/tiwai/salsa-lib

salsa-fetch: $(salsa_pkg)
salsa-unpack: $(salsa_dir)/.stamp-unpack
salsa-configure: $(salsa_dir)/.stamp-configure
salsa-build: $(salsa_dir)/.stamp-build
salsa-install: $(salsa_dir)/.stamp-install

$(salsa_pkg):
	curl -sSf $(salsa_url)/$(salsa_pkg) > $(salsa_pkg)+
	mv $(salsa_pkg)+ $(salsa_pkg)

$(salsa_dir)/.stamp-unpack: $(salsa_pkg)
	tar jxvf $(salsa_pkg)
	> $@

$(salsa_dir)/.stamp-configure: $(salsa_dir)/.stamp-unpack
	cd $(salsa_dir) && ./configure --prefix=$(prefix) --disable-mixer
	> $@

$(salsa_dir)/.stamp-build: $(salsa_dir)/.stamp-configure
	$(MAKE) -C $(salsa_dir)
	> $@

$(salsa_dir)/.stamp-install: $(salsa_dir)/.stamp-build
	$(MAKE) -C $(salsa_dir) install
	> $@

.PHONY: $(addprefix salsa-,fetch unpack configure build install)
