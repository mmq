# default target
all::

prefix = $(HOME)
bindir = $(prefix)/bin
INSTALL = install
STRIP ?= strip
GIT-VERSION-FILE: .FORCE-GIT-VERSION-FILE
	@./GIT-VERSION-GEN
-include GIT-VERSION-FILE

DISTNAME = mmq-$(GIT_VERSION)

LIBS = -lrt
EXTLIBS =

# overridable from command-line or config.mk
CFLAGS = -g -O2 -Wall
LDFLAGS = -Wl,-O1
MMQ_AO = pa

config.mk:
	@true
-include config.mk
include $(MMQ_AO).mk

OBJS := $(addsuffix .o,mmq-player print ao input)
HDRS = $(wildcard compat/*.h *.h)

ao.o: cflags = $(CFLAGS) $(MMQ_AO_CFLAGS)
ao.o: ao.c $(MMQ_AO).c mmq.h config.mk
	$(CC) -o $*.o -c $(cflags) -DMMQ_AO_IMPL='"$(MMQ_AO).c"' $<

FIXEDDEC_LIB := fixeddec/libfixeddec.a
FIXEDDEC_OBJS := $(addprefix fixeddec/,$(addsuffix .o,flac tremor mad))
$(FIXEDDEC_OBJS): fixeddec/fixeddec.h
$(FIXEDDEC_LIB): $(FIXEDDEC_OBJS)
	$(RM) $@
	$(AR) rcs $@ $(FIXEDDEC_OBJS)

%.o: %.c $(HDRS)
	$(CC) -o $*.o -c $(CFLAGS) $<

MMQ_DEC_LIBS := $(FIXEDDEC_LIB)

MMQ_BIN := mmq-player mmq-enq

mmq-enq: mmq-enq.c print.o
	$(CC) $(CFLAGS) -o $@ $^ $(LDFLAGS) $(LIBS)

mmq-player: ldflags = $(LDFLAGS) $(MMQ_AO_LDFLAGS)
mmq-player: libs = $(LIBS) $(MMQ_AO_LIBS) $(MMQ_DEC_LIBS)
mmq-player: $(OBJS) $(MMQ_DEC_LIBS)
	$(CC) $(CFLAGS) -o $@ $(OBJS) $(ldflags) $(libs) $(EXTLIBS)

all:: $(MMQ_BIN)

install: $(MMQ_BIN)
	$(INSTALL) -d -m 755 '$(DESTDIR)$(bindir)'
	$(INSTALL) $^ '$(DESTDIR)$(bindir)'

strip: $(MMQ_BIN)
	$(STRIP) $(STRIP_OPTS) $(MMQ_BIN)

install-strip: strip
	$(MAKE) install

dist: GIT-VERSION-FILE
	git archive --format=tar --prefix=$(DISTNAME)/ HEAD^{tree} \
		| gzip -9 > $(DISTNAME).tar.gz+
	mv $(DISTNAME).tar.gz+ $(DISTNAME).tar.gz
clean:
	$(RM) $(MMQ_BIN) $(DISTNAME).tar.gz* GIT-VERSION-FILE $(OBJS)
	$(RM) $(FIXEDDEC_OBJS) $(FIXEDDEC_LIB)

.PHONY: .FORCE-GIT-VERSION-FILE install
