/* command-line utility to enqueue tracks for mmq-player */

#define _XOPEN_SOURCE 600
#define _GNU_SOURCE

#include "compat/cc.h"
#include <assert.h>
#include <unistd.h>
#include <fcntl.h>
#include <sys/stat.h>
#include <mqueue.h>
#include <limits.h>
#include <stdlib.h>
#include <errno.h>
#include <string.h>
#include <sched.h>
#include <sys/time.h>
#include <sys/resource.h>

extern NORETURN void die(const char *tmp, ...) PRINTF;
extern void emit(const char *fmt, ...) PRINTF;
extern void warn(const char *fmt, ...) PRINTF;

static const char *usage = "Usage: %s [-p PRIO] [-n] [-s] [-c] [-P] FILE...\n";
#ifndef MMQ_MQUEUE
#  define MMQ_MQUEUE "/mmq"
#endif
static char path[PATH_MAX+2];
static int peek, clear, verbose;

static void * xmalloc(size_t size)
{
	void *rv = malloc(size);

	if (!rv)
		die("malloc: %s\n", strerror(errno));
	return rv;
}

struct peek_tmp {
	struct peek_tmp *next;
	unsigned int prio;
	size_t size;
	char buf[1];
};

static void set_mq_nonblock(mqd_t mq, int nb)
{
	struct mq_attr attr;

	attr.mq_flags = nb ? O_NONBLOCK : 0;

	if (mq_setattr(mq, &attr, NULL) < 0)
		die("mq_setattr(nonblock=%d): %s\n", nb, strerror(errno));
}

static void clear_or_peek(mqd_t mq, struct mq_attr *attr)
{
	ssize_t r;
	unsigned int pr;
	char *tmp = xmalloc(attr->mq_msgsize + 2);
	struct peek_tmp *peekhead = NULL;
	struct peek_tmp *peektail = NULL;
	struct peek_tmp *peektmp;
	int i;

	if (!(attr->mq_flags & O_NONBLOCK))
		set_mq_nonblock(mq, 1);

	for (i = 0; i < 30; i++) {
		while ((r = mq_receive(mq, tmp, attr->mq_msgsize, &pr)) >= 0) {
			/*
			 * successful receive, ensure we try again after
			 * we've sched_yield()-ed, below
			 */
			i = 0;

			if (verbose) {
				tmp[0] = '!';
				tmp[r] = '\n';
				write(1, tmp, r + 1);
			}
			if (peek) {
				tmp[0] = '=';
				tmp[r] = '\n';
				write(1, tmp, r + 1);

				peektmp = xmalloc(sizeof(struct peek_tmp) + r);

				tmp[0] = '=';
				peektmp->size = r;
				peektmp->prio = pr;
				peektmp->next = NULL;
				memcpy(peektmp->buf, tmp, r);
				if (peektail)
					peektail->next = peektmp;
				else
					peekhead = peektmp;
				peektail = peektmp;
			}
		}

		if (r < 0) {
			/*
			 * in case another process is blocking on mq_send(),
			 * let that process run so we can try mq_receive()
			 * what was just sent...
			 */
			if (errno == EAGAIN) {
				setpriority(PRIO_PROCESS, 0, 19);
				sched_yield();
			} else {
				break;
			}
		}
	}

	if (peek)
		set_mq_nonblock(mq, 0);
	while (peekhead) {
		if (mq_send(mq,
			    peekhead->buf,
			    peekhead->size,
			    peekhead->prio) < 0)
			die("mq_send: %s\n", strerror(errno));
		peektmp = peekhead->next;
		free(peekhead);
		peekhead = peektmp;
	}
	if (peek)
		set_mq_nonblock(mq, 1);

	if (!(attr->mq_flags & O_NONBLOCK))
		set_mq_nonblock(mq, 0);
	free(tmp);
}

int main(int argc, char * const argv[])
{
	int opt;
	int argi = 1;
	int abs_path = 1;
	char *mqueue = getenv("MQUEUE");
	mqd_t mq;
	int oflag = O_RDWR;
	unsigned prio = 0;
	struct mq_attr attr;

	if (!mqueue)
		mqueue = MMQ_MQUEUE;
	while ((opt = getopt(argc, argv, "p:nvscPh")) != -1) {
		unsigned long xprio;
		char *err;

		switch (opt) {
		case 'c':
			++argi;
			clear = 1;
			break;
		case 's':
			++argi;
			abs_path = 0;
			break;
		case 'v':
			++argi;
			++verbose;
			break;
		case 'n':
			++argi;
			oflag |= O_NONBLOCK;
			break;
		case 'p':
			argi += 2;
			xprio = strtoul(optarg, &err, 10);
			if (*err || xprio < 0 || xprio > UINT_MAX)
				die("prio must be an unsigned int\n");
			prio = (unsigned)xprio;
			break;
		case 'P':
			++argi;
			peek = 1;
			break;
		case 'h':
			emit(usage, argv[0]);
			return 0;
		default:
		fatal:
			die(usage, argv[0]);
		}
	}
	if (argi >= argc && ! clear && ! peek)
		goto fatal;

	if ((mq = mq_open(mqueue, oflag)) < 0)
		die("mq_open: %s\n", strerror(errno));
	if (mq_getattr(mq, &attr) < 0)
		die("mq_getattr: %s\n", strerror(errno));
	if (verbose)
		emit("MQUEUE=%s\n", mqueue);

	if (clear || peek)
		clear_or_peek(mq, &attr);

	for (; argi < argc; ++argi) {
		size_t len;

		if (abs_path) {
			if (realpath(argv[argi], path + 1) == NULL) {
				warn("realpath: %s\n", strerror(errno));
				write(2, argv[argi], strlen(argv[argi]));
				die("\n");
			}
		} else {
			len = strlen(argv[argi]);
			if (len > PATH_MAX) {
				warn("longer than PATH_MAX: %d\n", PATH_MAX);
				write(2, argv[argi], len);
				die("\n");
			}
			memcpy(path + 1, argv[argi], len);
		}

		path[0] = '=';
		len = strlen(path);
		if (verbose) {
			assert(path[len] == 0);
			path[len] = '\n';
			write(1, path, len + 1);
			path[len] = '\0';
		}
		if (mq_send(mq, path, len, prio) < 0) {
			warn("mq_send: %s\n", strerror(errno));
			write(2, path, len);
			die("\n");
		}
	}

	return 0;
}
