#ifndef MMQ_H
#define MMQ_H

/*
 * This includes OS-wide headers that can be expected to be available
 * on any machine that mmq can be compiled on for any POSIX-compliant OS
 *
 * This does not include headers for optional dependencies such as
 * those for input/output modules.
 */
#define _XOPEN_SOURCE 600
#define _GNU_SOURCE
/* #define _FILE_OFFSET_BITS 64 */  /* off by default */

#include "compat/cc.h"

#include <inttypes.h>
#include <sys/types.h>
#include <unistd.h>
#include <time.h>
#include <mqueue.h>
#include <fcntl.h>
#include <sys/stat.h>
#include <sys/time.h>
#include <stddef.h>
#include <stdlib.h>
#include <string.h>
#include <errno.h>
#include <assert.h>
#include <stdio.h>
#include <stdarg.h>
#include <signal.h>
#include <limits.h>

extern void emit(const char *err, ...) PRINTF;
extern void warn(const char *err, ...) PRINTF;
extern NORETURN void die(const char *err, ...) PRINTF;

enum player_state {
	STATE_PLAY = 0,
	STATE_NEXT = 1
};

extern enum player_state g_state;
extern long g_seek_msec;
extern unsigned long g_sample_size;

struct afmt {
	unsigned rate;
	unsigned bits;
	unsigned channels;
};

extern struct afmt g_next_afmt;
int audio_open(void);
void audio_close(int graceful);

typedef long (*conv_fn)(void *dst, void *src, long samples, long offset);
int audio_inject(void *buf, long samples, conv_fn conv_i);
long audio_copy(void *dst, void *src, long samples, long offset);

int input_open(const char *path);
off_t input_seek(off_t offset, int whence);
off_t input_size(void);
ssize_t input_read(void *buf, size_t count);
void *input_mmap(size_t *size);
void input_close(void);

#ifndef O_NOATIME
#  define O_NOATIME 0
#endif

#define ARRAY_SIZE(x) (sizeof(x)/sizeof(x[0]))

#if defined(__STDC_VERSION__) && __STDC_VERSION__ >= 199901L
#  define MMQ_C99 1
#else
#  ifdef NO_INLINE
#    define inline
#  endif
#  ifdef __GNUC__
#    define restrict __restrict
#  else
#    define restrict
#  endif
#  define MMQ_C99 0
#endif

#endif /* MMQ_H */
