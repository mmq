#ifndef COMPAT__CC_H
#define COMPAT__CC_H

/*
 * this allows us to take advantage of special gcc features while still
 * allowing other compilers to compile:
 *
 * example taken from: http://rlove.org/log/2005102601
 */

#if defined(__GNUC__) && (__GNUC__ >= 3)
#  define CONST __attribute__ ((const))
#  define DEPRECATED __attribute__ ((deprecated))
#  define MUST_CHECK __attribute__ ((warn_unused_result))
#  define NORETURN __attribute__ ((noreturn))
#  define PACKED __attribute__ ((packed))
/* these are very useful for type checking */
#  define PRINTF __attribute__ ((format(printf,1,2)))
#  define DPRINTF __attribute__ ((format(printf,2,3)))
#  define PURE __attribute__ ((pure))
#  define UNUSED __attribute__ ((unused))
#  define USED __attribute__ ((used))
/* #  define inline inline __attribute__ ((always_inline)) */
#  define NOINLINE __attribute__ ((noinline))
#  define likely(x) __builtin_expect (!!(x), 1)
#  define unlikely(x) __builtin_expect (!!(x), 0)
#else /* __GNUC__ || __GNUC__ < 3 */
#  define CONST
#  define DEPRECATED
#  define MALLOC
#  define MUST_CHECK
#  define NORETURN
#  define PACKED
#  define PRINTF
#  define DPRINTF
#  define PURE
#  define UNUSED
#  define USED
#  define NOINLINE
#  define likely(x) (x)
#  define unlikely(x) (x)
#endif /* __GNUC__ && __GNUC__ >= 3 */

#endif /* COMPAT__CC_H */
