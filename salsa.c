/*
 * audio output backend for SALSA-lib, a stripped-down version ALSA
 * http://www.alsa-project.org/main/index.php/SALSA-Library
 *
 * This file is meant to be included in ao.c
 */
#include <alsa/asoundlib.h>

#ifndef MMQ_ALSA_MODE
#  define MMQ_ALSA_MODE (SND_PCM_NO_AUTO_RESAMPLE| \
                         SND_PCM_NO_AUTO_CHANNELS| \
                         SND_PCM_NO_AUTO_FORMAT| \
                         SND_PCM_NO_SOFTVOL)
#endif

#ifndef MMQ_ALSA_ACCESS
#  define MMQ_ALSA_ACCESS SND_PCM_ACCESS_MMAP_INTERLEAVED
#endif

#ifndef MMQ_ALSA_BUFFER_TIME
#  define MMQ_ALSA_BUFFER_TIME 8000000
#endif
#ifndef MMQ_ALSA_PERIOD_TIME
#  define MMQ_ALSA_PERIOD_TIME 10000
#endif

static snd_pcm_t *pcm;
static snd_pcm_uframes_t period_size;

#ifndef MMQ_ALSA_DEVICE
#  define MMQ_ALSA_DEVICE "default"
#endif

#if MMQ_C99
#  define E(fn,...) \
     do { e = #fn; r = fn(__VA_ARGS__); if (r < 0) goto ex; } while (0)
#elif __GNUC__
#  define E(fn,va...) \
     do { e = #fn; r = fn(va); if (r < 0) goto ex; } while (0)
#else
#  error C99 or gcc needed for variadic macros
#endif /* ! C99 */

static snd_pcm_format_t bitformat(unsigned bits)
{
	switch (bits) {
	case 8: return SND_PCM_FORMAT_S8;
	case 16: return SND_PCM_FORMAT_S16;
	case 24: return SND_PCM_FORMAT_S24;
	case 32: return SND_PCM_FORMAT_S32;
	}
	return SND_PCM_FORMAT_UNKNOWN;
}

static int _my_open(struct afmt *fmt)
{
	static const char dev[] = MMQ_ALSA_DEVICE;
	snd_pcm_hw_params_t *hwp;
	const char *e;
	int r;
	snd_pcm_format_t bitfmt = bitformat(fmt->bits);
	unsigned val;

	if (bitfmt == SND_PCM_FORMAT_UNKNOWN) {
		warn("unknown bitrate: %u\n", fmt->bits);
		return -1;
	}
	E(snd_pcm_open, &pcm, dev, SND_PCM_STREAM_PLAYBACK, MMQ_ALSA_MODE);
	snd_pcm_hw_params_alloca(&hwp);
	E(snd_pcm_hw_params_any, pcm, hwp);
	E(snd_pcm_hw_params_set_access, pcm, hwp, MMQ_ALSA_ACCESS);
	E(snd_pcm_hw_params_set_format, pcm, hwp, bitfmt);

	val = fmt->channels;
	E(snd_pcm_hw_params_set_channels_near, pcm, hwp, &val);
	if (val != fmt->channels)
		warn("%s: wanted=%u got=%u\n", e, fmt->channels, val);
	g_sample_size = (fmt->bits / CHAR_BIT) * val;

	val = fmt->rate;
	E(snd_pcm_hw_params_set_rate_near, pcm, hwp, &val, NULL);
	if (val != fmt->rate)
		warn("%s: wanted=%u got=%u\n", e, fmt->rate, val);

	if ((val = MMQ_ALSA_BUFFER_TIME)) {
		E(snd_pcm_hw_params_set_buffer_time_near, pcm, hwp, &val, NULL);
		warn("ALSA buffer_time: %u => %u\n", MMQ_ALSA_BUFFER_TIME, val);
		emit("delay_ms=%u\n", val);
	}
	if ((val = MMQ_ALSA_PERIOD_TIME)) {
		E(snd_pcm_hw_params_set_period_time_near, pcm, hwp, &val, NULL);
		warn("ALSA period_time: %u => %u\n", MMQ_ALSA_PERIOD_TIME, val);
	}

	E(snd_pcm_hw_params_get_period_size, hwp, &period_size, NULL);
	E(snd_pcm_hw_params, pcm, hwp);
	E(snd_pcm_prepare, pcm);

	return 0;
ex:
	warn("%s: %s\n", e, snd_strerror(r));
	return -1;
}

static void _my_close(int graceful)
{
	if (!pcm)
		return;
	if (graceful && snd_pcm_state(pcm) == SND_PCM_STATE_RUNNING)
		snd_pcm_drain(pcm);
	snd_pcm_close(pcm);
	pcm = NULL;
}

#if MMQ_ALSA_ACCESS == SND_PCM_ACCESS_MMAP_INTERLEAVED
static void *
area_dst(const snd_pcm_channel_area_t *areas, snd_pcm_uframes_t offset, int i)
{
	unsigned char *tmp = areas[i].addr;

	assert(areas[i].first % 8 == 0 && "ALSA bug (areas.first % 8)");
	assert(areas[i].step % 8 == 0 && "ALSA bug (areas.step % 8)");

	return tmp + areas[i].first/8 + areas[i].step/8 * offset;
}

static int _my_inject(void *buf, long samples, conv_fn conv_i)
{
	snd_pcm_sframes_t r;
	snd_pcm_uframes_t offset;
	snd_pcm_uframes_t frames;
	const snd_pcm_channel_area_t *areas;
	long skip = 0;
	const char *e;
	snd_pcm_state_t state;
	int silent = 1;
	long rv = samples;

retry:
	state = snd_pcm_state(pcm);
	if (unlikely(state == SND_PCM_STATE_XRUN))
		E(snd_pcm_recover, pcm, -EPIPE, silent);
	if (unlikely(state == SND_PCM_STATE_SUSPENDED))
		E(snd_pcm_recover, pcm, -ESTRPIPE, silent);

	frames = samples;
	E(snd_pcm_avail_update, pcm);
	if ((snd_pcm_uframes_t)r < period_size) {
		E(snd_pcm_wait, pcm, -1);
		goto retry;
	}
	E(snd_pcm_mmap_begin, pcm, &areas, &offset, &frames);
	assert(frames <= (snd_pcm_uframes_t)samples && "overflow frames");

	{
		long nr;

		nr = conv_i(area_dst(areas, offset, 0), buf, frames, skip);
		if (unlikely(nr < 0)) {
			(void)snd_pcm_mmap_commit(pcm, offset, 0);
			return -1;
		}
		frames = nr;
	}

	E(snd_pcm_mmap_commit, pcm, offset, frames);
	if (unlikely(frames == 0))
		return 0; /* EOF */
	skip += frames;
	samples -= frames;
	if (unlikely(state == SND_PCM_STATE_PREPARED))
		E(snd_pcm_start, pcm);
	if (samples == 0)
		return rv;
	goto retry;
ex:
	warn("%s: %s\n", e, snd_strerror(r));
	return -1;
}

#elif MMQ_ALSA_ACCESS == SND_PCM_ACCESS_RW_INTERLEAVED

static int write_loop(char *vbuf, long samples)
{
	snd_pcm_sframes_t w;
	int tries = 5;
retry:
	w = snd_pcm_writei(pcm, vbuf, samples);
	if (w == samples)
		return 0;
	if (w < 0) {
		int err = (int)w;
		int silent = 1;

		warn("ALSA write: %s (trying recovery)\n", snd_strerror(err));
		err = snd_pcm_recover(pcm, err, silent);
		if (err < 0) {
			warn("ALSA recover: %s\n", snd_strerror(err));
			return -1;
		}
	} else {
		vbuf += w;
		samples -= w;
		if (w == 0 && !--tries)
			return -1;
	}
	goto retry;

	return 0;
}

static int _my_inject(void *buf, long samples, conv_fn conv_i)
{
	size_t bytes = samples * g_sample_size;
	static size_t capa;
	static void *tmp;
	long nr;

	if (bytes > capa) {
		capa = bytes;
		tmp = realloc(tmp, bytes);
		assert(tmp && "realloc failed, wtf");
	}
	nr = conv_i(tmp, buf, samples, 0);
	if (nr == 0) /* OV_HOLE */
		return 0;
	return write_loop(tmp, nr);
}
#endif /* MMQ_ALSA_ACCESS == SND_PCM_ACCESS_RW_INTERLEAVED */
