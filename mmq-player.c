#include "mmq.h"
#include "fixeddec/fixeddec.h"

#ifndef MMQ_MQUEUE
#  define MMQ_MQUEUE "/mmq"
#endif

#ifndef MMQ_MSGSIZE
#  define MMQ_MSGSIZE 256
#endif

#ifndef MMQ_MAXMSG
#  define MMQ_MAXMSG 10 /* Linux 2.6 default */
#endif

enum player_state g_state;
long g_seek_msec;
static struct mq_attr attr;
static mqd_t mq;

/*
 * we only have two opcodes for messages in the queue, signals handle
 * everything else
 */
enum opcode {
	OP_PLAY_PATH = '=', /* path to an audio file */
	OP_SEEK_PRE = ':' /* seek point for next song before playback */
};

static void next_handler(UNUSED int signum)
{
	g_state = STATE_NEXT;
	emit("^\n");
}

static void sig_init(void)
{
	struct sigaction sa;

	memset(&sa, 0, sizeof(struct sigaction));
	sigemptyset(&sa.sa_mask);
	sa.sa_flags = SA_RESTART;
	sa.sa_handler = next_handler;
	sigaction(SIGUSR1, &sa, NULL);
}

static void set_mq_nonblock(int nb)
{
	attr.mq_flags = nb ? O_NONBLOCK : 0;

	if (mq_setattr(mq, &attr, NULL) < 0)
		die("mq_setattr: %s\n", strerror(errno));
}

static void read_seek_point(char *buf, ssize_t len)
{
	buf[len] = '\0';
	g_seek_msec = strtol(buf, NULL, 10);

	emit(":%ld\n", g_seek_msec);
}

static void play_path(char *buf, ssize_t len)
{
	g_state = STATE_PLAY;
	buf[len] = '\n';
	write(STDOUT_FILENO, buf - 1, len + 2);
	buf[len] = '\0';
	fixeddec_play(buf, len);
	emit(".\n");
	if (g_state == STATE_NEXT)
		audio_close(0);
}

static void unknown(const char *buf, ssize_t len)
{
	warn("unknown message received:\n");
	write(2, buf, len);
	warn("\n-------------------------\n");
}

/* lets try to get away without accepting command-line arguments... */
int main(void)
{
	char tmp[MMQ_MSGSIZE + 1];
	ssize_t r;

	attr.mq_msgsize = MMQ_MSGSIZE;
	attr.mq_maxmsg = MMQ_MAXMSG;

	mq = mq_open(MMQ_MQUEUE, O_CREAT|O_RDONLY, 0666, &attr);
	if (mq == (mqd_t)-1)
		die("mq_open(%s): %s\n", MMQ_MQUEUE, strerror(errno));

	sig_init();
	for (;;) {
		r = mq_receive(mq, tmp, attr.mq_msgsize, NULL);
		if (r == 0 || (r < 0 && errno == EINTR)) {
			/* ignore */
		} else if (r < 0) {
			int err = errno;

			if (err == EAGAIN) {
				audio_close(1);
				set_mq_nonblock(0);
			} else {
				die("mq_receive: %s\n", strerror(err));
			}
		} else {
			assert(r > 0 && "r is not positive");
			switch (*tmp) {
			case OP_SEEK_PRE:
				read_seek_point(tmp + 1, r - 1);
				break;
			case OP_PLAY_PATH:
				play_path(tmp + 1, r - 1);
				set_mq_nonblock(1);
				break;
			default: unknown(tmp, r);
			}
		}
	}

	return 0;
}
