#include "mmq.h"

/*
 * not thread safe, we won't ever try to be
 * this buffer is intentionally tiny because we want to keep all
 * of our messages as short as possible.  For path names, we write()
 * to STDOUT_FILENO directly.
 */
static char buf[80];

static void spew(int fd, const char *fmt, va_list ap)
{
	int nr = vsnprintf(buf, sizeof(buf), fmt, ap);

	assert(nr > 0 && nr < sizeof(buf) && "spew buffer too small");
	write(fd, buf, nr);
}

void emit(const char *fmt, ...)
{
	va_list ap;

	va_start(ap, fmt);
	spew(1, fmt, ap);
	va_end(ap);
}

void warn(const char *fmt, ...)
{
	va_list ap;

	va_start(ap, fmt);
	spew(2, fmt, ap);
	va_end(ap);
}

NORETURN void die(const char *fmt, ...)
{
	va_list ap;

	va_start(ap, fmt);
	spew(2, fmt, ap);
	va_end(ap);
	exit(1);
}
